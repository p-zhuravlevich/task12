import React, {Component} from 'react';

class ButtonData extends Component {
    constructor(props) {
        super(props);
        this.state = {data: ["1 ", "2 ", "3 "]};
    }

    handleClick = (val) => {
        this.state.data.shift();
        this.state.data.push(val.target.value);
        this.setState({
            data: this.state.data
        })
    }

    render() {
        return (
            <React.Fragment>
                <div class="data">
                    {this.state.data}
                </div>
                <div>
                    <button value="1 " onClick={this.handleClick}>1</button>
                    <button value="2 " onClick={this.handleClick}>2</button>
                    <button value="3 " onClick={this.handleClick}>3</button>
                    <button value="4 " onClick={this.handleClick}>4</button>
                    <button value="5 " onClick={this.handleClick}>5</button>
                </div>
            </React.Fragment>
        )
    }
}

export default ButtonData;